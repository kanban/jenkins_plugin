<?php
/**
 * Comment
 **/
$credentials = [
    "login" => "guest",
    "password" => "guest",
    "host" => "rabbitmq",
    "port" => 5672,
];

$jenkinsHost     = getenv("JENKINS_HOST");
$jenkinsUser     = getenv("JENKINS_USER");
$jenkinsApiToken = getenv("JENKINS_API_TOKEN");

$connection = new AMQPConnection($credentials);
$reconnectAttempts = 0;
$reconnectInterval = 1;

do {
    try {
        $connection->connect();
    } catch (AMQPConnectionException $e) {
        $reconnectAttempts++;
        sleep($reconnectInterval);
    }
} while (!$connection->isConnected());

$channel = new AMQPChannel($connection);

$exchange = new AMQPExchange($channel);
$exchange->setName('amq.topic');
$exchange->setType(AMQP_EX_TYPE_TOPIC);
$exchange->setFlags(AMQP_DURABLE);

$e = new AMQPExchange($channel);
$e->setName('jenkins');
$e->setType(AMQP_EX_TYPE_TOPIC);
$e->setFlags(AMQP_DURABLE);
$e->declare();
$e->bind('amq.topic', 'board.#');

$queue = new AMQPQueue($channel);
$queue->setName('jenkins_trigger');
$queue->setFlags(AMQP_DURABLE);
$queue->declare();
$queue->bind('jenkins', '#');

$queue->consume(
    function($e, $q) use ($jenkinsHost, $jenkinsUser, $jenkinsApiToken) {
        $issue = json_decode($e->getBody(), true);
        $iid = $issue['data']['iid'];
        $pattern = "/KB\[stage\]\[\d\]\[(.*)\]/";
        preg_match($pattern, implode(' ', $issue['data']['labels']), $new);
        $project = $issue['data']['project']['data']['namespace']['name']
            . '_' . $issue['data']['project']['data']['name'] 
            . '_' . strtolower($new[1]);


        var_dump($issue);
        $data = [
            'parameter' => [
                    [
                        'name' => 'BRANCH',
                        'value' => 'issue_#' . $iid,
                    ]
                ]
            ];
        var_dump($project);
        $c = curl_init("https://{$jenkinsUser}:{$jenkinsApiToken}@${jenkinsHost}/job/{$project}/build");
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, ['json' => json_encode($data)]);
        curl_exec($c);
        curl_close($c);
        $q->ack($e->getDeliveryTag());
    }
);
