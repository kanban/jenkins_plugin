FROM leanlabs/php:1.1.1

ENV JENKINS_USER="jenkinsuser" \
    JENKINS_API_TOKEN="jenkinsuserapitoken" \
    JENKINS_HOST="ci.usercompany.com"

COPY ./bin/worker.php /app/worker.php

CMD ["php", "-e", "/app/worker.php"]
