Leanlabs.io Kanban Jenkins trigger plugin
=========================================

[![Join the chat at https://gitter.im/leanlabsio/kanban](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/leanlabsio/kanban?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Analytics](https://ga-beacon.appspot.com/UA-66361671-1/leanlabs/kanban)](https://github.com/igrigorik/ga-beacon)

Jenkins trigger plugin for [http://kanban.leanlabs.io](Leanlabs.io Kanban board). 
Listens for board move event and triggers appropriate jobs in Jenkins.
Jenkins jobs should follow naming conventions: **groupname_reponame_stage**, where:

**groupname** - name of project group in GitLab

**reponame**  - name of project repository in GitLab

**stage**     - name of stage on Kanban board.

For example, we have Kanban board for this repo composed of 3 stages: "Backlog", "Doing", "Done",
then when you move card from "Doing" to "Done" job with name "kanban_jenkins_plugin_done" will be triggered.

LeanLabs.io Kanban installation
-------------------------------

To enable plugin just add next lines to docker-compose.yml and restart containers:

```yaml
jenkins:
    image: leanlabs/jenkins-trigger
    links:
      - rabbitmq:rabbitmq
    environment:
      - JENKINS_USER=qwerty
      - JENKINS_API_TOKEN=qwerty
      - JENKINS_HOST=https://jenkins.com/
```
